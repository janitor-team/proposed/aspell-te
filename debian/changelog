aspell-te (0.01-2-7) unstable; urgency=low

  [ Kartik Mistry ]
  * Team upload.
  * Added debian/gitlab-ci.yml.
  * debian/control:
    + Updated Standards-Version to 4.5.0
    + Switched to debhelper-compat.
    + Added 'Rules-Requires-Root' field.

  [ Christopher Hoskin ]
  * debian/control:
    + Use new team e-mail address in control and copyright.
    + Change Section from text to localization.

 -- Kartik Mistry <kartik@debian.org>  Sun, 12 Apr 2020 10:12:38 +0530

aspell-te (0.01-2-6) unstable; urgency=medium

  * Bump compat from 5 to 11
  * Update VCS to Salsa
  * Use secure copyright format uri
  * Update debian/copyright
  * Use secure URI for debian/watch
  * Remove whitespace from debian/changelog
  * Bump Standards-Version from 3.9.3 to 4.3.0.2 (no change required)
  * Add Autopkgtest test
  * Use dh_aspell-simple
  * Add myself to Uploaders

 -- Christopher Hoskin <mans0954@debian.org>  Mon, 11 Feb 2019 18:56:00 +0000

aspell-te (0.01-2-5) unstable; urgency=low

  [Kartik Mistry]
  * Team upload.
  * debian/control:
    + Updated Vcs-* fields for Git.
    + Bumped Standards-Version to 3.9.3
  * debian/copyright:
    + Updated to use copyright-format 1.0

 -- Kartik Mistry <kartik@debian.org>  Mon, 19 Mar 2012 22:40:34 +0530

aspell-te (0.01-2-4) unstable; urgency=low

  [Vasudev Kamath]
  * debian/rules:
    + Excluded var/lib/aspell from md5sum calculation. (Closes: #638739)
  * debian/copyright:
    + Fixed dep5 Format URL.

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 05 Nov 2011 18:17:25 +0530

aspell-te (0.01-2-3) unstable; urgency=low

  [Vasudev Kamath]
  * debian/control:
    + Bumped Standards-Version to 3.9.2 (no changes needed)
    + Removed cdbs dependency
  * debian/copyright:
    + Updated to latest DEP-5 specifications
  * debian/rules:
    + Changed from cdbs to dh7 build system

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sun, 05 Jun 2011 11:17:06 +0530

aspell-te (0.01-2-2) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.8.4 (no changes needed)
    + Wrapped up Depends and Build-Depends
  * Use source format 3.0 (quilt)
  * debian/copyright:
    + Updated as per DEP-5 specifications

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 15 May 2010 08:10:28 +0530

aspell-te (0.01-2-1) unstable; urgency=low

  [Kartik Mistry]
  * New upstream release
  * debian/copyright:
    + Don't use versionless symlink to license text

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Wed, 05 Aug 2009 11:43:40 +0530

aspell-te (0.01-4) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.8.2 (no changes needed)
    + [Lintian] Added ${misc:Depends} for debhelper dependency
    + Updated extented package description
  * debian/copyright:
    + Updated to use correct copyright symbol

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Wed, 24 Jun 2009 09:51:40 +0530

aspell-te (0.01-3) unstable; urgency=low

  [Kartik Mistry]
  * Added debian/watch file
  * debian/copyright:
    + Moved copyright out of license section
    + Updated link of GPL
  * debian/control:
    + Updated Hompage entry as real control field
    + Updated package descriptions
    + Updated Standards-Version to 3.7.3
    + Added VCS-* fields
  * debian/changelog:
    + Fixed typo of compatibility from previous upload

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 26 Jan 2008 14:53:10 +0530

aspell-te (0.01-2) unstable; urgency=low

  [Kartik Mistry]
  * Set maintainer address to Debian-IN Team
  * Updated standards-version to 3.7.2
  * Updated debhelper compatibility to 5
  * debian/copyright: updated according to standard copyright
  * debian/control: fixed build-dependency, fixed long descriptions
  * Removed useless debian/README.Debian
  * debian/changelog: removed useless empty line at end

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Mon, 21 May 2007 12:59:33 +0530

aspell-te (0.01-1) unstable; urgency=low

  * Initial Release.(Closes:#330732)

 -- Soumyadip Modak <soumyadip@softhome.net>  Thu, 29 Sep 2005 21:18:08 +0530
